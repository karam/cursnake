# cursenake

**Cursnake** is a simple TUI *snake* game written in Python.


## Play

**Cursnake** needs no external dependency:

```sh
python3 cursnake.py
```

### Control keys

*NOTE: Letters are case-insensitive*

- `hjkl` or `wasd` or arrow keys to control the snake.
- *SPACE* key to reverse the snake (in addition to reversing via control keys).
- `q` to manually lose the game
- `t` to disable/enable teleporting the wall
- `r` to disable/enable reversing the snake
- `-` and `=` to decrease or increase the game speed, respectively.
- `9` and `0` to decrease or increase the apple density, respectively.


## License

**Cursnake** is licensed under the [MIT (Expat) license](./LICENSE).

> Copyright (C) 2021 Karam Assany (karam.assany@disroot.org)
