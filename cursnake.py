#!/usr/bin/env python3
#
# cursnake.py: simple curses snake game written in Python
#
#
# Copyright (C) 2021 Karam Assany <karam.assany@disroot.org>
#
# Licensed under the MIT (Expat) license.  See LICENSE
#


import curses as cu

from random import randrange
from time   import time



## Constants


# Appearance

COLOR_THEME = {
        "body"  : (cu.COLOR_CYAN  , cu.COLOR_CYAN),
        "head"  : (cu.COLOR_RED   , cu.COLOR_RED),
        "apple" : (cu.COLOR_YELLOW, cu.COLOR_BLACK),
        "wall"  : (cu.COLOR_WHITE , cu.COLOR_WHITE),
        "dwall" : (cu.COLOR_BLUE  , cu.COLOR_BLUE ),
        "bar"   : (cu.COLOR_BLACK , cu.COLOR_GREEN)
        }


# Directions

DOWN  = ( 1,  0)
UP    = (-1,  0)
RIGHT = ( 0,  1)
LEFT  = ( 0, -1)


# Control keys (letters)

MOVE_KEYS = {
        DOWN : (cu.KEY_DOWN , *(ord(k) for k in ("s", "S", "j", "J"))),
        UP   : (cu.KEY_UP   , *(ord(k) for k in ("w", "W", "k", "K"))),
        RIGHT: (cu.KEY_RIGHT, *(ord(k) for k in ("d", "D", "l", "L"))),
        LEFT : (cu.KEY_LEFT , *(ord(k) for k in ("a", "A", "h", "H")))
        }

SPEED_KEYS = {
        ord("-"):  1,
        ord("="): -1
    }

DENSITY_KEYS = {
        ord("9"): -5,
        ord("0"):  5
    }

EXIT_KEYS = tuple(ord(k) for k in ("q", "Q", "x", "X"))

FLAG_REVERSABLE = tuple(ord(k) for k in ("r", "R"))
FLAG_TELEPORT   = tuple(ord(k) for k in ("t", "T"))

REVERSE_KEY = ord(" ")



## Functions


def main_screen(ss):

    # Useful functions
    
    # Gets the color pair number based on its name
    cp = lambda pair_name : cu.color_pair(tuple(COLOR_THEME).index(pair_name)
            + 1)
    
    # Moves a dot in a given direction
    mv = lambda dot, sdir : (dot[0] + sdir[0], dot[1] + sdir[1])

    # Reverses a direction
    rv = lambda sdir : {LEFT: RIGHT, RIGHT: LEFT, UP: DOWN, DOWN: UP}[sdir]

    flag = lambda flag : "ON" if flag else "OFF"

    # Initializing basic variables

    frame_delay   = 10 # 1: fastest, 10: one-second
    apple_density = 51

    flag_teleport   = False  # Enables teleporting the wall
    flag_reversable = True   # Enables reversing the snake body

    # Initializing curses

    # Setting initial maximum frame delay
    cu.halfdelay(frame_delay)

    # Hides the cursor
    cu.curs_set(0)

    # Defines color pairs
    for num, pair_name in enumerate(COLOR_THEME, start = 1):
        cu.init_pair(num, *COLOR_THEME[pair_name])

    real_height, width = ss.getmaxyx()
    height = real_height - 1

    # Initializing game variables
    
    score = frames = 0

    # sdir stores the current snake direction
    sdir = DOWN

    # Initializing game objects

    # Apples are generated inside the game loop
    apples = []

    # Generating initial snake
    snake  = [ ( height // 3, width // 2 ) ]
    for i in range(3): snake.append(mv(snake[-1], rv(sdir)))

    # Rendering initial snake
    ss.attron(cu.A_BOLD)
    ss.addch(*snake[0], "O", cp("head"))
    for dot in snake[1:]: ss.addch(*dot, "O", cp("body"))
    ss.attroff(cu.A_BOLD)
    
    # Generating wall

    wall = []

    for n in range(height):
        wall.append( (n, 0        ) )
        wall.append( (n, width - 1) )

    for n in range(width):
        wall.append( (0          , n) )
        wall.append( (height - 1 , n) )

    wall = tuple(wall)

    # Rendering the wall
    for dot in wall: ss.addch(*dot, "X", cp("dwall") if flag_teleport else
            cp("wall"))

    # Marking the game beginning
    start_time = time()

    # The game loop

    while True:
        
        frames += 1

        # Generating apples

        apple_limit = ( ( height * width - len(wall) - len(snake) ) //
            ( apple_density * 10 - 8 ))

        while len(apples) < apple_limit:
            # Create needed apples

            dot = (randrange(1, height - 1), randrange(1, width - 1))
            if dot not in (*apples, *wall, *snake):
                apples.append(dot)
                ss.addch(*dot, str(randrange(10)), cp("apple"))

        while len(apples) > apple_limit:
            # Delete surplus apples

            del_apple = apples[randrange(len(apples))]
            ss.addch(*del_apple, " ")
            apples.remove(del_apple)

        # Creating a new head
        new_head = mv(snake[0], sdir)

        if new_head in wall:
            
            if flag_teleport:  # Teleporting to the other side
                if   new_head[0] == 0:
                    new_head = (height - 2, new_head[1])
                elif new_head[0] == height - 1:
                    new_head = (1         , new_head[1])
                elif new_head[1] == 0:
                    new_head = (new_head[0], width - 2)
                elif new_head[1] == width - 1:
                    new_head = (new_head[0], 1        )
            else:
                return ("Bumped into wall", score, frames, time() - start_time)
            
        if new_head in snake:
            return ("Bumped into the snake itself", score, frames, time() -
                    start_time)


        if new_head in apples:
            # Eat the apple
            apples.remove(new_head)
            score += int(chr(ss.inch(*new_head) & 0xFF))
            ss.addch(*new_head, " ")

        else:
            # Remove the tail
            ss.addch(*snake.pop(), " ")

        # Appending the new head

        ss.attron(cu.A_BOLD)
        ss.addch(*snake[0], "O", cp("body"))  # Converting old head into body
        ss.addch(*new_head, "O", cp("head"))
        ss.attroff(cu.A_BOLD)

        snake.insert(0, new_head)

        # Writing the statusbar

        bar = ("Teleport? %s | Reversable? %s | Speed: %d/10 | Apple scarcity: "
            "%d/15 | Score: %d") % (
                flag(flag_teleport), flag(flag_reversable), 11 - frame_delay,
                (apple_density + 4) / 5, score)
        bar = bar + " " * (width - len(bar) - 2)

        ss.attron(cu.A_UNDERLINE)
        ss.addstr(height, 1, bar, cp("bar"))
        ss.attroff(cu.A_UNDERLINE)

        # Refreshing the frame
        ss.refresh()

        # Catching input

        key = ss.getch()

        if key in EXIT_KEYS:
            return ("Ended by user", score, frames, time() - start_time)

        if key in SPEED_KEYS:
            frame_delay += SPEED_KEYS[key]
            if frame_delay > 10: frame_delay = 10  # max val
            if frame_delay <  1: frame_delay =  1  # min val
            cu.halfdelay(frame_delay)
            
        if key in DENSITY_KEYS:
            apple_density += DENSITY_KEYS[key]
            if apple_density >  71: apple_density = 71  # max val
            if apple_density <   1: apple_density =  1  # min val

        if key in FLAG_REVERSABLE: flag_reversable = not flag_reversable

        if key in FLAG_TELEPORT:
            flag_teleport = not flag_teleport
            # Regenerate the wall (for coloring effects)
            for dot in wall: ss.addch(*dot, "X", cp("dwall") if flag_teleport
                    else cp("wall"))

        old_dir = sdir

        for dr in (DOWN, UP, RIGHT, LEFT):
            if key in MOVE_KEYS[dr]:
                sdir = dr

        if (key == REVERSE_KEY) or (sdir == rv(old_dir)):
            if flag_reversable:
                # Reversing the snake body
                snake = snake[::-1]
                sdir = (snake[0][0] - snake[1][0], snake[0][1] - snake[1][1])
            else:
                # Revert back to old dir
                sdir = old_dir


if __name__ == "__main__":
    
    try:
        exit_report = cu.wrapper(main_screen)

    except KeyboardInterrupt:
        exit_report = "Interrupted by user"

    finally:
        try:
            report = exit_report
        except NameError:
            report = "Unknown reason"

        if type(report) == str:
            print("Game stooped: " + report)
        else:
            print("Game over.\n" +
                    "Reason:       %s\n"   % report[0] +
                    "Score:        %d\n"   % report[1] +
                    "Spent frames: %d\n"   % report[2] +
                    "Play time:    %0.0fs" % report[3])
